# Shrio学习笔记
## SpringBoot-Shrio
**学习环境**
> IDEA 2020
>
> SpringBoot 2.3.0
>
>Shiro 1.5.3
>
>Thymeleaf - shiro 2.0.0

### 1.Shrio核心三大对象
- Subject  用户
- SecurityManager  管理用户
- Realm  连接数据
### 2.引入整合包
#### 2.1 SpringBoot整合Shiro
```xml
        <dependency>
            <groupId>org.apache.shiro</groupId>
            <artifactId>shiro-spring</artifactId>
            <version>1.5.3</version>
        </dependency>
        <dependency>
            <groupId>org.apache.shiro</groupId>
            <artifactId>shiro-core</artifactId>
            <version>1.5.3</version>
        </dependency>
```
#### 2.2 Thymeleaf整合Shiro
```xml
         <dependency>
            <groupId>com.github.theborakompanioni</groupId>
            <artifactId>thymeleaf-extras-shiro</artifactId>
            <version>2.0.0</version>
        </dependency>
```
### 3.编写配置类
- 自定义Realm对象

作用：用于授权和认证。
验证登录权限，获取并设置登录用户的权限。

- ShiroConfig

拿到自定义的Realm对象，并将其绑定到安全管理器中，主要在安全管理器中设置拦截、设置权限、登录校验等操作。


