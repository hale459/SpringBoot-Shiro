package com.lethe;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.lethe.dao")
public class SpringbootWebShiroApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootWebShiroApplication.class, args);
    }

}
