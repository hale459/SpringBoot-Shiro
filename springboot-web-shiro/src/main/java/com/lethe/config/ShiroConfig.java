package com.lethe.config;

import at.pollux.thymeleaf.shiro.dialect.ShiroDialect;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.LinkedHashMap;
import java.util.Map;

@Configuration
public class ShiroConfig {

    /**
     * 1.创建自定义Realm对象
     *
     * @return 自定义Realm对象
     */
    @Bean
    public UserReaml userReaml() {
        return new UserReaml();
    }

    /**
     * 2.关联上自定义的Realm对象
     *
     * @param userReaml
     * @return Shrio安全管理器
     */
    @Bean(name = "securityManager")
    public DefaultWebSecurityManager getDefaultWebSecurityManager(UserReaml userReaml) {
        DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();

        //关联自定义的Realm
        securityManager.setRealm(userReaml);

        return securityManager;
    }

    /**
     * 3.ShiroFilterBean
     *
     * @param defaultWebSecurityManager
     * @return 过滤后的bean
     */
    @Bean
    public ShiroFilterFactoryBean getShiroFilterFactoryBean(@Qualifier("securityManager") DefaultWebSecurityManager defaultWebSecurityManager) {
        ShiroFilterFactoryBean bean = new ShiroFilterFactoryBean();

        //设置安全管理器
        bean.setSecurityManager(defaultWebSecurityManager);

        /*
        ==添加shiro内置过滤器==
        anno：无需认证可访问
        authc：必须认证才能访问
        user：必须拥有记住我功能才可使用
        perms：拥有对某个资源的权限
        role：拥有某个角色权限下能访问
         */

        //配置拦截
        Map<String, String> filterChainDefinitionMap = new LinkedHashMap<>();

        //设置只有含有"user:page3"权限的人才能访问page3
        filterChainDefinitionMap.put("/page3", "perms[user:page3]");

        //设置只要登录了就可以访问的页面(所有人)
        filterChainDefinitionMap.put("/page2", "authc");
        filterChainDefinitionMap.put("/page1", "authc");

        bean.setFilterChainDefinitionMap(filterChainDefinitionMap);

        //登录时跳转的页面(也就是登录校验)
        bean.setLoginUrl("/toLogin");

        //登陆成功后跳转的页面
        bean.setSuccessUrl("/index");

        //没有授权时跳转到此页面
        bean.setUnauthorizedUrl("/Unauthorized");

        return bean;
    }

    /**
     * Thymeleaf整合Shiro
     *
     * @return
     */
    @Bean
    public ShiroDialect getShiroDialect() {
        return new ShiroDialect();
    }
}
