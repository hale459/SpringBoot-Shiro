package com.lethe.controller;

import com.lethe.entity.User;
import com.lethe.service.UserService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class IndexController {
    @Autowired
    private UserService userService;

    /**
     * 跳转主页
     *
     * @return 主页
     */
    @RequestMapping({"/", "/index", "/index.html"})
    public String toIndex() {
        return "index";
    }

    /**
     * 跳转页面1
     *
     * @return 页面1
     */
    @RequestMapping("/page1")
    public String page1() {
        return "page1";
    }

    /**
     * 跳转页面2
     *
     * @return 页面2
     */
    @RequestMapping("/page2")
    public String page2() {
        return "page2";
    }

    /**
     * 跳转页面3
     *
     * @return 页面3
     */
    @RequestMapping("/page3")
    public String page3() {
        return "page3";
    }

    /**
     * 跳转登陆页面
     *
     * @return 登录页
     */
    @RequestMapping("/toLogin")
    public String toLogin() {
        return "login";
    }

    /**
     * 登录验证操作
     *
     * @param username
     * @param password
     * @param model
     * @return 成功：主页；失败：登录页
     */
    @PostMapping("/login")
    public String login(String username, String password, Model model) {
        //获取当前用户
        Subject subject = SecurityUtils.getSubject();

        //封装用户登录数据到Token令牌中
        UsernamePasswordToken token = new UsernamePasswordToken(username, password);

        //设置记住我功能
        //token.setRememberMe(true);

        try {
            //执行登录操作：如果没有异常就到主页，否则异常并返回到登录页
            subject.login(token);
            model.addAttribute("token", token);
            model.addAttribute("msg", "你好，" + token.getUsername() + "!");
            return "index";
        } catch (UnknownAccountException e) {
            model.addAttribute("errorInfo", "用户名错误！");
            return "login";
        } catch (IncorrectCredentialsException e) {
            model.addAttribute("errorInfo", "密码错误！");
            return "login";
        }
    }

    /**
     * 若未授权时，会跳到此页面
     *
     * @return 未授权页面
     */
    @RequestMapping("/Unauthorized")
    @ResponseBody
    public String unauthorized() {
        return "未授权无法访问!";
    }

    @RequestMapping("/loginOut")
    public String loginOut() {
        SecurityUtils.getSubject().logout();
        return "login";
    }

    /**
     * 跳转注册页
     *
     * @return 注册页
     */
    @RequestMapping("/toReg")
    public String toReg() {
        return "reg";
    }

    /**
     * 用户注册校验
     *
     * @param username
     * @param password
     * @param model
     * @return
     */
    @PostMapping("/reg")
    @Transactional(rollbackFor = Exception.class)
    public String reg(String username, String password, Model model) {
        int reg = userService.reg(new User(null, username, password, null));
        if (reg == 1) {
            return "login";
        } else {
            model.addAttribute("error", "注册失败，请重试！");
            return "reg";
        }

    }

}
