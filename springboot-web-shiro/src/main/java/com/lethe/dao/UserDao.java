package com.lethe.dao;

import com.lethe.entity.User;

public interface UserDao {

    //登录校验
    User login(String username);

    //注册
    int reg(User user);

}
