package com.lethe.service;

import com.lethe.entity.User;

public interface UserService {

    //登录校验
    User login(String username);

    //注册
    int reg(User user);
}
