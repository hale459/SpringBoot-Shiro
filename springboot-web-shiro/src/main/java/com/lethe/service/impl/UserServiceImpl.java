package com.lethe.service.impl;

import com.lethe.dao.UserDao;
import com.lethe.entity.User;
import com.lethe.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public User login(String username) {
        return userDao.login(username);
    }

    @Override
    public int reg(User user) {
        return userDao.reg(user);
    }
}
